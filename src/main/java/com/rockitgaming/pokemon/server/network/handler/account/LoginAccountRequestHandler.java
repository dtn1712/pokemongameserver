package com.rockitgaming.pokemon.server.network.handler.account;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.util.MapperUtils;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.SocketCommandType;
import com.rockitgaming.pokemon.network.socket.SocketErrorCode;
import com.rockitgaming.pokemon.network.socket.handler.BaseValidateInputRequestHandler;
import com.rockitgaming.pokemon.network.socket.message.MessageFactory;
import com.rockitgaming.pokemon.server.network.GameErrorCode;
import com.rockitgaming.pokemon.service.AccountService;
import com.rockitgaming.pokemon.service.exceptions.AccountAccessException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LoginAccountRequestHandler extends BaseValidateInputRequestHandler {

    private static final Logger logger = LogManager.getLogger(RegisterAccountRequestHandler.class);

    @Autowired
    private AccountService accountService;

    public LoginAccountRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    protected void handleRequest(Session session, Message message, Map<String, Object> messageData) {
        if (session.isAuthenticated()) {
            writeMessage(session, MessageFactory.createErrorMessage(GameErrorCode.ERROR_ALREADY_LOGIN,
                    String.format("This session %s from IP %s already login", session.getSessionId(), session.getClientIp())));
            return;
        }

        try {
            ObjectMapper objectMapper = MapperUtils.getObjectMapper();
            String loginType = (String) messageData.get("loginType");
            String loginValue = (String) messageData.get("loginValue");
            String password = (String) messageData.get("password");
            Account account = accountService.login(loginType, loginValue, password, session.getClientIp());
            Message successfulMessage = MessageFactory.createMessage(SocketCommandType.COMMAND_SUCCESS);
            successfulMessage.putString(Message.DATA_KEY, objectMapper.writeValueAsString(account));
            session.setAuthenticated(true);
            session.setLoginTime(account.getLastSuccessLoginTime());
            session.setLoginUsername(account.getUsername());
            writeMessage(session, successfulMessage);
        } catch (AccountAccessException e) {
            logger.error("Failed to login account", e);
            writeMessage(session, MessageFactory.createErrorMessage(GameErrorCode.ERROR_LOGIN_ACCOUNT, e.getMessage()));
        } catch (JsonProcessingException e) {
            logger.error("Failed to write output", e);
            writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_HANDLE_REQUEST,"Failed to handle request. Please try again"));
        }

    }
}
