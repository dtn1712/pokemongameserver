package com.rockitgaming.pokemon.server.network;

/**
 * Game Command Type start with 100 till 999
 */
public class GameCommandType {

    public static final short COMMAND_REGISTER = 100;
    public static final short COMMAND_LOGIN = 101;
    public static final short COMMAND_LOGOUT = 102;

}
