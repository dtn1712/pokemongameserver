package com.rockitgaming.pokemon.server.network.handler.account;


import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.server.network.handler.BaseAuthorizeSessionRequestHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ChoosePlayerRequestHandler extends BaseAuthorizeSessionRequestHandler {

    public ChoosePlayerRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    public void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData) {

    }
}
